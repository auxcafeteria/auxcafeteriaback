import { NextFunction, Request, Response, Router } from "express";
import { signIn, signUp } from "../controllers/auth.controllers";
import {
  checkDuplicateEmail,
  checkRolesExisted,
} from "../middlewares/verifySignUp";

const router: Router = Router();

router.use((req: Request, res: Response, next: NextFunction) => {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.route("/signin").post(signIn);

router.route("/signup").post([checkDuplicateEmail, checkRolesExisted], signUp);

export default router;
