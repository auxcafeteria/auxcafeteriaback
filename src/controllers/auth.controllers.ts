import { Request, Response } from "express";
import bcryptjs from "bcryptjs";

import { IUser } from "../models/types/user.types";
import { createToken } from "../libs/createToken";
import Roles from "../models/roles.model";
import User from "../models/users.model";

export const signUp = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { password, roles } = req.body;

    const newUser: IUser = new User(req.body);

    // Encrypt password
    const salt: string = await bcryptjs.genSalt(10);
    newUser.password = await bcryptjs.hash(password, salt);

    // checking for roles
    if (roles) {
      const foundRoles = await Roles.find({ name: { $in: roles } });

      newUser.roles = foundRoles.map((role) => role._id);
    } else {
      // if the role parameter does not exist
      const role: any = await Roles.findOne({ name: "user" });
      newUser.roles = [role._id];
    }

    //Save new user
    const savedUser: IUser = await newUser.save();

    return res
      .status(200)
      .json({ message: "SignUp successfully", token: createToken(savedUser) });
  } catch (error) {
    return res.status(400).json({ error });
  }
};

export const signIn = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { email, password } = req.body;

    const userFound: IUser | null = await User.findOne({ email }).populate(
      "roles"
    );
    if (!userFound) return res.status(404).json({ error: "User not Found" });

    //check if password matches
    const correctPassword = await bcryptjs.compare(
      password,
      userFound.password
    );

    if (!correctPassword)
      return res.status(400).json({ error: "The password isn´t incorrect" });

    return res
      .status(200)
      .json({ message: "SignIn successfully", token: createToken(userFound) });
  } catch (error) {
    return res.status(400).json({ error });
  }
};
