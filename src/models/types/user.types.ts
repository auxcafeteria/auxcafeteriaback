import { Document } from "mongoose";

export interface IUser extends Document {
  firstName: string;
  lastName: string;
  phoneNumber: number;
  address: string;
  email: string;
  password: string;
  roles: any;
}
