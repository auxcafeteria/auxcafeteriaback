export interface IConfig {
  PORT: string;
  NODE_ENV: string;
  DATABASE_URI: string;
  JWT_TOKEN: string;
}
