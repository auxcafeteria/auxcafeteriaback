import Role from "../models/roles.model";
import User from "../models/users.model";
import bcrypt from "bcryptjs";

export const createRoles = async () => {
  try {
    // Count Documents
    const count: number = await Role.estimatedDocumentCount();

    // check for existing roles
    if (count > 0) return;

    // Create default Roles
    await Promise.all([
      new Role({ name: "user" }).save(),
      new Role({ name: "cashier" }).save(),
      new Role({ name: "admin" }).save(),
    ]);
  } catch (error) {
    console.error(error);
  }
};

export const createAdmin = async () => {
  try {
    // check for an existing admin user
    const user = await User.findOne({ email: "admin@colab.com" });
    // get roles _id
    const roles = await Role.find({ name: { $in: ["admin", "cashier"] } });

    if (user) return;

    // create a new admin user
    await User.create({
      firstName: "Aux",
      lastName: "Colab",
      phoneNumber: 23134224,
      email: "admin@colab.com",
      password: await bcrypt.hash("admin@colab.com", 10),
      roles: roles.map((role) => role._id),
    });
  } catch (error) {
    console.error(error);
  }
};
