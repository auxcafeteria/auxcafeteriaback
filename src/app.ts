import express, { Application } from "express";
import compression from "compression";
import helmet from "helmet";
import cors from "cors";
import config from "./config";
import morgan from "morgan";

import authRoutes from "./routes/auth.routes";
import welcomeRoutes from "./routes/welcome.routes";

import { createRoles, createAdmin } from "./libs/intialSetup";

const app: Application = express();
createRoles();
createAdmin();

//Settings
app.set("port", parseInt(config.PORT));

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Routes
app.use(welcomeRoutes);
app.use("/api", authRoutes);

export default app;
