import { connect, connection, ConnectionOptions } from "mongoose";
import config from "./config";

export const startConnection = async (): Promise<void> => {
  try {
    const connectOption: ConnectionOptions = {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    };

    await connect(config.DATABASE_URI, connectOption);
    console.log("Database is connected!");
    console.log(`Database name: ${connection.name}`);
  } catch (error) {
    console.error(error);
  }
};
